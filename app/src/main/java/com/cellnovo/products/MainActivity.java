package com.cellnovo.products;

import  android.content.res.Resources;
import  android.os.Build;
import  android.os.Bundle;
import  android.support.v7.app.AppCompatActivity;
import  android.support.v7.widget.RecyclerView;
import  android.support.v7.widget.Toolbar;
import  android.util.TypedValue;
import  android.view.View;
import  android.view.ViewGroup;
import  android.view.ViewTreeObserver;
import  android.widget.TextView;

import  com.cellnovo.products.Product;
import  com.cellnovo.products.os.AsyncTaskGetProducts;

import  java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements
    AsyncTaskGetProducts.OnSuccessListener,
    ViewTreeObserver.OnPreDrawListener {
    
    final String url = "https://www.bitbytec.co.uk/celltest/product/read.php";
    
    private ViewGroup activity;
    private RecyclerView recyclerview;
    private Toolbar toolbar;
    private TextView txt_products;
    
    // Adjust any views obstructed by the translucent status on Lollipop and newer.
    private void adjustIfNeeded(View[] views) {
    
        // We are only using windowTranslucentStatus on lollipop and newer.
        if (Build.VERSION.SDK_INT >= 21) {
        
            TypedValue val = new TypedValue();
            
            Resources.Theme theme = getTheme();
            theme.resolveAttribute(android.R.attr.windowTranslucentStatus, val, true);
            
            if (val.data == 0) {
                return;
            }
            
            // Get the status bar height identifier.
            final int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
            
            if (resourceId > 0) {
            
                // Get the status bar height.
                final int height = getResources().getDimensionPixelSize(resourceId);
                
                // Adjust our views padding.
                for (View view : views) {
                
                    if (view != null) {
                        view.setPaddingRelative(view.getPaddingStart(), view.getPaddingTop() + height, view.getPaddingEnd(), view.getPaddingBottom());
                    }
                
                }
            
            }
        
        }
    
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        activity = findViewById(R.id.activity_main);
        activity.getViewTreeObserver().addOnPreDrawListener(this);
        
        toolbar = activity.findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        adjustIfNeeded(new View[] { toolbar });
        
        recyclerview = activity.findViewById(R.id.recyclerview);
        txt_products = activity.findViewById(R.id.txt_products);
    
    }
    
    @Override
    public boolean onPreDraw() {
    
        activity.getViewTreeObserver().removeOnPreDrawListener(this);
        
        AsyncTaskGetProducts task = new AsyncTaskGetProducts();
        task.addOnSuccessListener(this);
        task.execute(url);
        
        return true;
    
    }
    
    @Override
    public void onSuccess(ArrayList<Product> products) {
        txt_products.setText(String.valueOf(products.size()));
    }

}