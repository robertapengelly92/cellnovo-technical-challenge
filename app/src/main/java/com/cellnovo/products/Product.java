package com.cellnovo.products;

public class Product {

    private String cat_id;
    private String cat_name;
    private String desc;
    private String id;
    private String name;
    private String price;
    
    public Product() {}
    
    public String getCategoryId() {
        return cat_id;
    }
    
    public String getCategoryName() {
        return cat_name;
    }
    
    public String getDescription() {
        return desc;
    }
    
    public String getId() {
        return id;
    }
    
    public String getName() {
        return name;
    }
    
    public String getPrice() {
        return price;
    }
    
    public void setCategoryId(String cat_id) {
        this.cat_id = cat_id;
    }
    
    public void setCategoryName(String cat_name) {
        this.cat_name = cat_name;
    }
    
    public void setDescription(String desc) {
        this.desc = desc;
    }
    
    public void setId(String id) {
        this.id = id;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setPrice(String price) {
        this.price = price;
    }

}