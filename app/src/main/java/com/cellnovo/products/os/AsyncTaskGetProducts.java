package com.cellnovo.products.os;

import  android.os.AsyncTask;

import  com.cellnovo.products.Product;

import  java.io.BufferedReader;
import  java.io.InputStreamReader;
import  java.net.HttpURLConnection;
import  java.net.URL;
import  java.util.ArrayList;

import  org.json.JSONArray;
import  org.json.JSONObject;

public class AsyncTaskGetProducts extends AsyncTask<String, String, ArrayList<Product>> {

    private ArrayList<OnSuccessListener> mListeners;
    
    public void addOnSuccessListener(OnSuccessListener listener) {
    
        if (mListeners == null) {
            mListeners = new ArrayList<> ();
        }
        
        mListeners.add(listener);
    
    }
    
    @Override
    protected ArrayList<Product> doInBackground(String... params) {
    
        URL url;
        HttpURLConnection conn = null;
        
        try {
        
            // Specify the URL to connect to.
            // Note: We assume the url was passed as the first argument in params.
            url = new URL(params[0]);
            
            // Create the connection and connect.
            conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setDoOutput(true);
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.connect();
            
            // Get the contents of the result.
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
            
            String line;
            StringBuilder sb = new StringBuilder();
            
            // Loop through the content and add it our StringBuilder.
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            
            // Close the buffer.
            br.close();
            
            // Get the contents as a string.
            String json = sb.toString();
            
            // Create a new JSONObject from the content.
            JSONObject obj = new JSONObject(json);
            JSONArray records = obj.getJSONArray("records");
            
            Product product;
            ArrayList<Product> products = new ArrayList<>();
            
            for (int i = 0; i < records.length(); i++) {
            
                obj = records.getJSONObject(i);
                
                product = new Product();
                product.setCategoryId(obj.getString("category_id"));
                product.setCategoryName(obj.getString("category_name"));
                product.setDescription(obj.getString("description"));
                product.setId(obj.getString("id"));
                product.setName(obj.getString("name"));
                product.setPrice(obj.getString("price"));
                
                products.add(product);
            
            }
            
            return products;
        
        } catch (Exception ex) {
        
            if (conn != null) {
                conn.disconnect();
            }
            
            return null;
        
        }
    
    }
    
    @Override
    protected void onPostExecute(ArrayList<Product> products) {
        super.onPostExecute(products);
        
        if (products != null) {
        
            if (mListeners != null) {
            
                for (OnSuccessListener listener : mListeners) {
                    listener.onSuccess(products);
                }
            
            }
        
        }
    
    }
    
    public void removeOnSuccessListener(OnSuccessListener listener) {
    
        if (mListeners == null) {
            return;
        }
        
        mListeners.remove(listener);
        
        if (mListeners.size() == 0) {
            mListeners = null;
        }
    
    }
    
    public interface OnSuccessListener {
        void onSuccess(ArrayList<Product> products);
    }

}